/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/06 15:31:27 by qdam              #+#    #+#             */
/*   Updated: 2021/06/15 17:52:51 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include <stdbool.h>
# include "mlx.h"

# define WINW	800
# define WINH	600

# ifdef __APPLE__
#  define KEY_ESC	53
#  define KEY_LEFT	123
#  define KEY_RIGHT	124
#  define KEY_DOWN	125
#  define KEY_UP	126
# elif __linux__
#  define KEY_ESC	65307
#  define KEY_LEFT	65361
#  define KEY_RIGHT	65363
#  define KEY_DOWN	65364
#  define KEY_UP	65362
# endif

# define MOUSEW_U	4
# define MOUSEW_D	5

typedef unsigned int	t_uint;

typedef struct s_data
{
	void	*prog;
	void	*win;
	void	*img;
	char	*addr;
	int		bpp;
	int		line;
	int		end;
	double	offx;
	double	offy;
	double	zoom;
}	t_data;

typedef struct s_im
{
	double	r;
	double	i;
}	t_im;

int		on_destroy_win(t_data *data);
int		on_mouse(int button, int x, int y, t_data *data);
int		on_key(int key, t_data *data);

double	norm_sq(t_im num);
double	norm(t_im num);
void	iter_quad(t_im *z, t_im c);

t_uint	create_tgrb(t_uint t, t_uint r, t_uint g, t_uint b);
void	px_to_im(t_data *data, int x, int y, t_im *num);
void	put_px(int x, int y, t_uint color, t_data *data);

void	julia(t_data *data, bool psyc);
void	mandel(t_data *data, bool psyc);

#endif
