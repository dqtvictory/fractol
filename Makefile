INC_DIR		= includes
SRC_DIR		= srcs

NAMES		= events.c julia.c main.c mandel.c math.c window.c
SRCS		= $(addprefix $(SRC_DIR)/, $(NAMES))

OBJS		= $(SRCS:.c=.o)

NAME		= fractol

CC			= clang
CFLAGS		= -Wall -Wextra -Werror -O3

RM			= rm -f

ifeq ($(shell uname), Darwin)
MLX_DIR		= ./mlx_mac
MLX_FLAGS	= -framework OpenGL -framework AppKit
else
MLX_DIR		= ./mlx_linux
MLX_FLAGS	= -lX11 -lXext -lm -lz
endif

.c.o:
			$(CC) $(CFLAGS) -I$(INC_DIR) -c $< -o $(<:.c=.o)

$(NAME):	$(OBJS)
			make -C $(MLX_DIR)
			$(CC) $(CFLAGS) $(OBJS) -I$(INC_DIR) -L$(MLX_DIR) -lmlx $(MLX_FLAGS) -o $(NAME)

all:		$(NAME)

clean:
			$(RM) $(OBJS)
			make -C $(MLX_DIR) clean

fclean:		clean
			$(RM) $(NAME)
			make -C $(MLX_DIR) fclean

re:			fclean all

.PHONY:		all clean fclean re