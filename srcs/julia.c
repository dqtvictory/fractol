/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 16:49:22 by qdam              #+#    #+#             */
/*   Updated: 2021/06/15 17:51:03 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#define ITER 256
#define DEFAULT_CR -0.8
#define DEFAULT_CI 0.156

typedef struct s_julia
{
	t_data	*d;
	bool	psyc;
	t_im	z;
	t_im	c;
}	t_julia;

static void	draw(t_julia *fig)
{
	int		x;
	int		y;
	int		i;
	t_uint	color;

	x = -1;
	while (++x < WINW)
	{
		y = -1;
		while (++y < WINH)
		{
			i = 0;
			px_to_im(fig->d, x, y, &fig->z);
			while (++i < ITER && norm_sq(fig->z) < 4.0)
				iter_quad(&fig->z, fig->c);
			if (fig->psyc)
				color = create_tgrb(100, i << 2, i << 1, i << 0);
			else
				color = create_tgrb(0, i, i, i);
			put_px(x, y, color, fig->d);
		}
	}
	mlx_put_image_to_window(fig->d->prog, fig->d->win, fig->d->img, 0, 0);
}

static int	on_loop(t_julia *fig)
{
	static double	zoom = 0.0;
	static double	offx = 0.0;
	static double	offy = 0.0;
	t_data			*d;

	d = fig->d;
	if (zoom != d->zoom || offx != d->offx || offy != d->offy)
	{
		zoom = d->zoom;
		offx = d->offx;
		offy = d->offy;
		draw(fig);
	}
	return (0);
}

void	julia(t_data *data, bool psyc)
{
	t_julia	fig;

	fig.d = data;
	fig.psyc = psyc;
	fig.c.r = DEFAULT_CR;
	fig.c.i = DEFAULT_CI;
	mlx_loop_hook(data->prog, on_loop, &fig);
	mlx_loop(data->prog);
}
