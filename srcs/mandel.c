/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/12 14:38:01 by qdam              #+#    #+#             */
/*   Updated: 2021/06/15 17:52:10 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#define ITER 256
#define DEFAULT_ZR 0.0
#define DEFAULT_ZI 0.0

typedef struct s_mandel
{
	t_data	*d;
	bool	psyc;
	t_im	z;
	t_im	c;
}	t_mandel;

static void	draw(t_mandel *fig)
{
	int		x;
	int		y;
	int		i;
	t_uint	color;

	x = -1;
	while (++x < WINW)
	{
		y = -1;
		while (++y < WINH)
		{
			i = ITER;
			fig->z.r = DEFAULT_ZR;
			fig->z.i = DEFAULT_ZI;
			px_to_im(fig->d, x, y, &fig->c);
			while (--i && norm_sq(fig->z) < 4.0)
				iter_quad(&fig->z, fig->c);
			if (fig->psyc)
				color = create_tgrb(100, i << 1, i << 3, i << 4);
			else
				color = create_tgrb(0, i, i, i);
			put_px(x, y, color, fig->d);
		}
	}
	mlx_put_image_to_window(fig->d->prog, fig->d->win, fig->d->img, 0, 0);
}

static int	on_loop(t_mandel *fig)
{
	static double	zoom = 0.0;
	static double	offx = 0.0;
	static double	offy = 0.0;
	t_data			*d;

	d = fig->d;
	if (zoom != d->zoom || offx != d->offx || offy != d->offy)
	{
		zoom = d->zoom;
		offx = d->offx;
		offy = d->offy;
		draw(fig);
	}
	return (0);
}

void	mandel(t_data *data, bool psyc)
{
	t_mandel	fig;

	fig.d = data;
	fig.psyc = psyc;
	mlx_loop_hook(data->prog, on_loop, &fig);
	mlx_loop(data->prog);
}
