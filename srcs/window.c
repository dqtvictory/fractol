/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/11 16:46:02 by qdam              #+#    #+#             */
/*   Updated: 2021/06/15 17:52:26 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_uint	create_tgrb(t_uint t, t_uint r, t_uint g, t_uint b)
{
	return (t << 24 | r << 16 | g << 8 | b);
}

void	px_to_im(t_data *d, int x, int y, t_im *num)
{
	const int		half_w = WINW / 2;
	const int		half_h = WINH / 2;
	static double	z = 0.0;

	if (z != d->zoom)
		z = d->zoom;
	num->r = 1.5 * (((x - d->offx) - half_w) / (z * half_w) - d->offx);
	num->i = 1.0 * (((y - d->offy) - half_h) / (z * half_h) - d->offy);
}

void	put_px(int x, int y, t_uint color, t_data *data)
{
	char	*dst;

	dst = data->addr + (y * data->line + x * (data->bpp / 8));
	*(t_uint *)dst = color;
}
