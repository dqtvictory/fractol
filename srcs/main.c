/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/06 15:33:08 by qdam              #+#    #+#             */
/*   Updated: 2021/06/13 17:03:26 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static bool	str_equal(char *s1, char *s2)
{
	while (*s1 || *s2)
		if (*s1++ != *s2++)
			return (false);
	return (true);
}

static bool	str_in_tab(char **tab, char *str)
{
	while (*tab)
		if (str_equal(*tab++, str))
			return (true);
	return (false);
}

static int	display_help(char **choices)
{
	printf("Invalid command! ");
	printf("2nd argument must be one of the following:\n");
	while (*choices)
		printf("- %s\n", *choices++);
	printf("3rd argument is optional. Enter -p ");
	printf("for psychedelic effects.\n");
	return (1);
}

static void	init_data(t_data *d)
{
	d->prog = mlx_init();
	d->win = mlx_new_window(d->prog, WINW, WINH, "Frac'tol");
	d->img = mlx_new_image(d->prog, WINW, WINH);
	d->addr = mlx_get_data_addr(d->img, &d->bpp, &d->line, &d->end);
	d->offx = 0;
	d->offy = 0;
	d->zoom = 1.0;
	mlx_hook(d->win, 4, 1L << 2, on_mouse, d);
	mlx_hook(d->win, 2, 1L << 0, on_key, d);
	mlx_hook(d->win, 17, 0L, on_destroy_win, d);
}

int	main(int ac, char **av)
{
	static char	*choices[] = {"julia", "mandel", NULL};
	t_data		data;

	if ((ac != 2 && ac != 3) || !str_in_tab(choices, av[1]))
		return (display_help(choices));
	if (ac == 3 && !str_equal("-p", av[2]))
		return (display_help(choices));
	init_data(&data);
	if (str_equal(choices[0], av[1]))
		julia(&data, ac == 3);
	else if (str_equal(choices[1], av[1]))
		mandel(&data, ac == 3);
}
