/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/12 13:29:25 by qdam              #+#    #+#             */
/*   Updated: 2021/06/13 16:59:26 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	on_destroy_win(t_data *data)
{
	mlx_destroy_image(data->prog, data->img);
	exit(0);
}

int	on_mouse(int button, int x, int y, t_data *data)
{
	(void)(x + y);
	if (button == MOUSEW_D && data->zoom > 1.0)
	{
		data->zoom *= 0.95;
		if (data->zoom < 1.0)
			data->zoom = 1.0;
	}
	else if (button == MOUSEW_U)
		data->zoom /= 0.95;
	return (0);
}

int	on_key(int key, t_data *data)
{
	if (key == KEY_ESC)
	{
		mlx_destroy_image(data->prog, data->img);
		mlx_destroy_window(data->prog, data->win);
		exit(0);
	}
	else if (key == KEY_LEFT)
		data->offx += 0.01;
	else if (key == KEY_RIGHT)
		data->offx -= 0.01;
	else if (key == KEY_UP)
		data->offy += 0.01;
	else if (key == KEY_DOWN)
		data->offy -= 0.01;
	return (0);
}
