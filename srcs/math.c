/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/12 13:26:53 by qdam              #+#    #+#             */
/*   Updated: 2021/06/15 17:53:06 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

double	norm_sq(t_im num)
{
	return (num.r * num.r + num.i + num.i);
}

double	norm(t_im num)
{
	return (sqrt(norm_sq(num)));
}

void	iter_quad(t_im *z, t_im c)
{
	double	tmp;

	tmp = z->r * z->r - z->i * z->i;
	z->i = 2 * z->r * z->i + c.i;
	z->r = tmp + c.r;
}
